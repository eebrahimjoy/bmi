import 'package:bmi/contants/contant.dart';
import 'package:bmi/screens/input/input_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(BMICalculator());
}

class BMICalculator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: kPrimaryColor,
          scaffoldBackgroundColor: kPrimaryColor,
      ),
      home: InputScreen(),
    );
  }
}



